package filkom.nim195150400111004.restapitest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InsertActivity extends AppCompatActivity {

    EditText etJudul, etDeskripsi;
    Button btnSave;
    PerpustakaanService perpustakaanService;
    String judul, deskripsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        etJudul = findViewById(R.id.etJudul);
        etDeskripsi = findViewById(R.id.etDeskripsi);
        btnSave = findViewById(R.id.btnSave);
        perpustakaanService = RetrofitClient.getClient().create(PerpustakaanService.class);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<Buku> addRequest = perpustakaanService.addBuku(etJudul.getText().toString(), etDeskripsi.getText().toString());
                addRequest.enqueue(new Callback<Buku>() {
                    @Override
                    public void onResponse(Call<Buku> call, Response<Buku> response) {
                        startActivity(new Intent(InsertActivity.this, MainActivity.class));
                    }

                    @Override
                    public void onFailure(Call<Buku> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}