package filkom.nim195150400111004.restapitest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView rvBuku;
    ArrayList<Buku> listBuku;
    BukuAdapter adapter;
    PerpustakaanService perpustakaanService;
    Button btnAdd;
    public static MainActivity ma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listBuku = new ArrayList<>();
        adapter = new BukuAdapter(this, listBuku);
        rvBuku = findViewById(R.id.rvBuku);
        rvBuku.setLayoutManager(new LinearLayoutManager(this));
        rvBuku.setAdapter(adapter);
        btnAdd = findViewById(R.id.btnAdd);
        perpustakaanService = RetrofitClient.getClient().create(PerpustakaanService.class);
        ma = this;
        Call<List<Buku>> listRequest = perpustakaanService.listBuku();
        listRequest.enqueue(new Callback<List<Buku>>() {
            @Override
            public void onResponse(Call<List<Buku>> call, Response<List<Buku>> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "success", Toast.LENGTH_SHORT).show();
                    List<Buku> list = response.body();
                    Log.d("success", "list" + list.size());
                    listBuku.addAll(list);
                    adapter.notifyDataSetChanged();
                } else {
                    Log.d("errt", "" + response.errorBody());
                    Toast.makeText(MainActivity.this, "" + response.errorBody(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Buku>> call, Throwable t) {
                Log.d("DataModel", "" + t.getMessage());
                Toast.makeText(getApplicationContext(), "Error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, InsertActivity.class));
            }
        });
    }
}